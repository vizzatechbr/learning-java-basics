# README #

Aprenda o básico do básico

### Conteudo ###

* Operações e Tipos Numéricos
* Operações Lógicas
* Exemplos de Iterações
* Teste JUnit para validar sua codificação dos exercícios

### Começando ###

* [Importe esse projeto no Eclipse](https://bitbucket.org/vizzatechbr/learning-java-basics/wiki/Home) e comece a aprender!
* Execute a classe PlayMe para uma introdução rápida
* Abra os arquivos Exercicios1a5 e Exercicios6a10. Cada método é um exercício

### Executando JUnit para validar os testes ###
* Abra a classe correspondente do JUnit no Eclipse (Exercicios1a5Test por exemplo)
* Pressione o botão "Run" e identifique o resultado
* Corrija se houver erros, senão, Parabéns!