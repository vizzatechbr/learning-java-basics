package br.com.vizza.interactivelearning.numerico;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.vizza.interactivelearning.exercicio.Exercicios1a5;

public class Exercicios1a5Test {

	private Exercicios1a5 ex = new Exercicios1a5();

	@Test
	public void testIsNumeroPar() {
		for (int numero = 2; numero < 100; numero += 2) {
			assertTrue(ex.isNumeroPar(numero));
		}

		for (int numero = 1; numero < 100; numero += 2) {
			assertFalse(ex.isNumeroPar(numero));
		}
	}

	@Test
	public void testIsNumeroImpar() {
		for (int numero = 1; numero < 100; numero += 2) {
			assertTrue(ex.isNumeroImpar(numero));
		}

		for (int numero = 2; numero < 100; numero += 2) {
			assertFalse(ex.isNumeroImpar(numero));
		}
	}

	@Test
	public void testIsNumeroEntre1e100() {
		assertTrue(ex.isNumeroEntre1e100(50));
		assertTrue(ex.isNumeroEntre1e100(99));
		assertFalse(ex.isNumeroEntre1e100(101));
	}

	@Test
	public void testIsNumeroEntre1e100Negativo() {
		assertFalse(ex.isNumeroEntre1e100Negativo(0));
		assertTrue(ex.isNumeroEntre1e100Negativo(-30));
	}

}
