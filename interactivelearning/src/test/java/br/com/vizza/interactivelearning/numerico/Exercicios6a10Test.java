package br.com.vizza.interactivelearning.numerico;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.vizza.interactivelearning.exercicio.Exercicios6a10;

public class Exercicios6a10Test {

	private Exercicios6a10 ex = new Exercicios6a10();

	@Test
	public void testMaiorNumero() {
		assertEquals(100, ex.maiorNumero(1, 2, 3, 4, 5, 6, 7, 8, 92, 100));
	}

	@Test
	public void testMenorNumero() {
		assertEquals(-100, ex.menorNumero(1, 2, 3, 4, 5, 6, 7, 8, 92, -100));
	}

	@Test
	public void testContagemNumerosIguais() {
		assertEquals(4,
				ex.contagemNumerosIguais(4, 1, 2, 3, 4, 4, 4, 5, 6, 5, 4, 7));
	}

	@Test
	public void testContagemNumerosMaiores() {
		assertEquals(4,
				ex.contagemNumerosMaiores(4, 1, 2, 3, 4, 4, 4, 5, 6, 5, 4, 7));
	}

	@Test
	public void testContagemNumerosMenores() {
		assertEquals(3,
				ex.contagemNumerosMenores(4, 1, 2, 3, 4, 4, 4, 5, 6, 5, 4, 7));
	}

}
