package br.com.vizza.interactivelearning.exemplo.iteracoes;

public class TiposIteracoes {

	public void iteracaoWhile(int loops) {
		int contador = 0;
		while (contador < loops) {
			System.out.println("While loop: " + contador);
			contador++;
		}
	}

	public void iteracaoFor(int loops) {
		for (int i = 0; i < loops; i++) {
			System.out.println("For loop: " + i);
		}
	}

	public void iteracaoDoWhile(int loops) {
		int contador = 0;
		do {
			System.out.println("Do While loop: " + contador);
			contador++;
		} while (contador < loops);
	}

	public void iteracaoForComBreak(int loops, int loopBreak) {
		// O break funciona em qualquer operação
		for (int i = 0; i < loops; i++) {
			System.out.println("For loop com break: " + i);
			if (i >= loopBreak) {
				System.out.println("Break na contage: " + i + ", esperado: "
						+ loopBreak);
				break;
			}
		}
	}

	public static void main(String[] args) {
		TiposIteracoes tiposIteracoes = new TiposIteracoes();
		tiposIteracoes.iteracaoDoWhile(10);
		tiposIteracoes.iteracaoFor(10);
		tiposIteracoes.iteracaoDoWhile(10);
		tiposIteracoes.iteracaoForComBreak(10, 4);
	}
}
