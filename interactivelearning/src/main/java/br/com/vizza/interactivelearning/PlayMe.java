package br.com.vizza.interactivelearning;

import java.util.Scanner;

import br.com.vizza.interactivelearning.exemplo.iteracoes.TiposIteracoes;
import br.com.vizza.interactivelearning.exemplo.logica.OperacoesLogicas;
import br.com.vizza.interactivelearning.exemplo.numerico.OperacoesETiposNumericos;

public class PlayMe {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		OperacoesETiposNumericos.main(null);
		//		pausarProg();
		OperacoesLogicas.main(null);
		TiposIteracoes.main(null);
	}

	public static void pausarProg() {
		System.out.println("Pressione Enter para continuar...");
		Scanner keyboard = new Scanner(System.in);
		keyboard.nextLine();
	}

}
