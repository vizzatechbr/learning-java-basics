package br.com.vizza.interactivelearning.exemplo.logica;

import javax.swing.JOptionPane;

public class OperacoesLogicas {

	private static final String MSG_LAYOUT = "Comparando se %s e %s são %s: %s ";
	private static final String MSG_LAYOUT_2 = "Comparando se %s é %s que %s :%s ";

	public String exemploIgualdade(int a, int b) {
		return this.print("" + a, "" + b, "iguais", "" + (a == b));
	}

	public String exemploDiferenca(int a, int b) {
		return this.print("" + a, "" + b, "diferentes", "" + (a != b));
	}

	public String exemploMaior(int a, int b) {
		return this.printMaiorMenor("" + a, "maior", "" + b, "" + (a > b));
	}

	public String exemploMenor(int a, int b) {
		return this.printMaiorMenor("" + a, "menor", "" + b, "" + (a < b));
	}

	public String exemploMaiorIgual(int a, int b) {
		return this.printMaiorMenor("" + a, "maior ou igual", "" + b, "" + (a >= b));
	}

	public String exemploMenorOuIgual(int a, int b) {
		return this.printMaiorMenor("" + a, "menor ou igual", "" + b, "" + (a <= b));
	}

	public String operacaoDentroDeIf(int a, int b, int c) {
		StringBuilder builder = new StringBuilder(1024);
		builder.append("============= operacaoDentroDeIf =============\n");

		if (a == b) {
			builder.append("a igual à b\n");
		} else {
			builder.append("a diferente de b\n");
		}

		if (a == c) {
			builder.append("a igual à c\n");
		} else {
			builder.append("a diferente de c\n");
		}

		if (b == c) {
			builder.append("b igual à c\n");
		} else {
			builder.append("b diferente de c\n");
		}

		if (a != c && a != b && b != c) {
			builder.append("Nenhum dos números são iguais\n");
		}

		return builder.toString();
	}

	private String print(String... a) {
		return (String.format(MSG_LAYOUT, a)) + "\n";
	}

	private String printMaiorMenor(String... a) {
		return String.format(MSG_LAYOUT_2, a) + "\n";
	}

	private String printOperadoresLogicos() {
		StringBuilder builder = new StringBuilder(1024);
		builder.append("============= printOperadoresLogicos =============\n");
		builder.append("'==' verifica igualdade\n");
		builder.append("\t if(a==b) { }\n\n");
		builder.append("'!=' verifica diferença\n");
		builder.append("\t if(a!=b) { }\n\n");
		builder.append("'>' verifica se é maior\n");
		builder.append("\t if(a>b) { }\n\n");
		builder.append("'<' verifica se é menor\n");
		builder.append("\t if(a<b) { }\n\n");
		builder.append("'>' verifica se é maior ou igual\n");
		builder.append("\t if(a>=b) { }\n\n");
		builder.append("'<' verifica se é menor ou igual\n");
		builder.append("\t if(a<=b) { }\n\n");
		builder.append("'&&' operação lógica de AND (E) [todas as condições ligadas por &&(AND) devem obedecer à condição imposta (serem verdadeiras))\n");
		builder.append("\t (1 < 2 && 4 < 3) = false\n");
		builder.append("\t (1 < 2 && 4 > 3) = true\n\n");
		builder.append("'||' operação lógica de OR (OU) [ao menos uma condição ligada por ||(OR) deve obedecer à condição imposta (serem verdadeiras))\n");
		builder.append("\t (1 < 2 || 4 < 3) = true\n");
		builder.append("\t (1 < 2 && 4 < 3) = false\n");
		return builder.toString();
	}

	public static void main(String[] args) {
		StringBuilder builder = new StringBuilder(4096);
		OperacoesLogicas operacoesLogicas = new OperacoesLogicas();
		builder.append(operacoesLogicas.exemploIgualdade(2, 2));
		builder.append(operacoesLogicas.exemploDiferenca(2, 2));
		builder.append(operacoesLogicas.exemploMaior(6, 4));
		builder.append(operacoesLogicas.exemploMaiorIgual(6, 6));
		builder.append(operacoesLogicas.exemploMenor(4, 89));
		builder.append(operacoesLogicas.exemploMenorOuIgual(4, 5));
		builder.append(operacoesLogicas.operacaoDentroDeIf(1, 3, 3));
		builder.append(operacoesLogicas.printOperadoresLogicos());
		JOptionPane.showMessageDialog(null, builder.toString());
	}
}
