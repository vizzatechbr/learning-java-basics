package br.com.vizza.interactivelearning.exemplo.numerico;

import javax.swing.JOptionPane;

public class OperacoesETiposNumericos
{

	private static final String	MSG_LAYOUT	= "%s de %d com %d, resultando em: ";
	private StringBuilder		bldr		= new StringBuilder(128);

	/**
	 * Método para operação de Soma (mais)
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int exemploSoma(int a, int b)
	{
		this.print("Soma", a, b, a + b);
		return a + b;
	}

	/**
	 * Método para operação de Subtração (menos)
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int exemploSubtracao(int a, int b)
	{
		this.print("Subatração", a, b, a - b);
		return a - b;
	}

	/**
	 * Método para operação de multiplicação
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int exemploMultiplicacao(int a, int b)
	{
		this.print("Multiplicação", a, b, a * b);
		return a * b;
	}

	/**
	 * Método para operação de divisão
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int exemploDivisao(int a, int b)
	{
		this.print("Divisão", a, b, (double) a / b);
		return a / b;
	}

	/**
	 * Método para efetuar operação para mostrar o resto de uma divisão Ex: 4
	 * divido por 5 = resto 4
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public int exemploRestoDivisao(int a, int b)
	{
		this.print("Resto de Divisão", a, b, a % b);
		return a % b;
	}

	public void printNuméricos()
	{
		this.bldr.append("\nTipos de Numéricos:\n___________________\n");
		this.bldr.append("Tipo short (Short) min: ").append(Short.MIN_VALUE).append(", max: ").append(Short.MAX_VALUE)
		.append("\n");
		this.bldr.append("\tEx: short s = 1;").append("\n\n");
		this.bldr.append("Tipo int (Integer) min: " + Integer.MIN_VALUE + ", max: " + Integer.MAX_VALUE).append("\n");
		this.bldr.append("\tEx: int i = 1;").append("\n\n");
		this.bldr.append("Tipo long (Long) min: " + Long.MIN_VALUE + ", max: " + Long.MAX_VALUE).append("\n");
		this.bldr.append("\tEx: long l = 99999999999L;").append("\n\n");
		this.bldr.append("Tipo double (Double) min: " + Double.MIN_VALUE + ", max: " + Double.MAX_VALUE).append("\n");
		this.bldr.append("\tEx: double d = 0.314d;").append("\n\n");
		this.bldr.append("Tipo float (Float) min: " + Float.MIN_VALUE + ", max: " + Float.MAX_VALUE).append("\n");
		this.bldr.append("\tEx: float f = 0.314f;").append("\n");
	}

	private void print(String oper, int a, int b, Number c)
	{
		this.bldr.append(String.format(MSG_LAYOUT, oper, a, b) + c).append("\n");
	}

	public static void main(String[] args)
	{
		int a = 4;
		int b = 5;
		OperacoesETiposNumericos operadoresNumericos = new OperacoesETiposNumericos();
		operadoresNumericos.exemploSoma(a, b);
		operadoresNumericos.exemploSubtracao(a, b);
		operadoresNumericos.exemploMultiplicacao(a, b);
		operadoresNumericos.exemploDivisao(a, b);
		operadoresNumericos.exemploRestoDivisao(a, b);
		operadoresNumericos.bldr
		.append("A operação de divisão pode vir zerada, pois retornam um primitivo inteiro (não possui casa decimal)")
		.append("\n");
		operadoresNumericos.bldr.append("No nosso caso não virá, pois estamos fazendo CAST(convertendo) para double")
		.append("\n");
		operadoresNumericos.printNuméricos();
		JOptionPane.showMessageDialog(null, operadoresNumericos.bldr.toString());
	}
}
