package br.com.vizza.interactivelearning.exercicio;

public class Exercicios1a5 {

	/**
	 * Exercício 1: Esse método deve verificar se o número recebido é par
	 * 
	 * @param numero
	 * @return
	 */
	public boolean isNumeroPar(int numero) {
		// TODO seu código aqui
		return false;
	}

	/**
	 * Exercício 2: Esse método deve verificar se o número recebido é impar
	 * 
	 * @param numero
	 * @return
	 */
	public boolean isNumeroImpar(int numero) {
		// TODO seu código aqui
		return false;
	}

	/**
	 * Exercício 3: Esse método deve verificar se o número recebido está entre 1
	 * e 100
	 * 
	 * @param numero
	 * @return
	 */
	public boolean isNumeroEntre1e100(int numero) {
		// TODO seu código aqui
		return false;
	}

	/**
	 * Exercício 4: Esse método deve verificar se o número recebido está entre
	 * -100 e -1
	 * 
	 * @param numero
	 * @return
	 */
	public boolean isNumeroEntre1e100Negativo(int numero) {
		// TODO seu código aqui
		return false;
	}
}
