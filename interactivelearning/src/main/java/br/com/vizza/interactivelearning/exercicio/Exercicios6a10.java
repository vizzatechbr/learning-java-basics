package br.com.vizza.interactivelearning.exercicio;

public class Exercicios6a10 {

	/**
	 * Exercício 6: Esse método deve receber uma coleção de números e retornar o
	 * maior
	 * 
	 * @param numero
	 * @return
	 */
	public int maiorNumero(Integer... numeros) {
		// TODO seu código aqui
		return Integer.MIN_VALUE;
	}

	/**
	 * Exercício 7: Esse método deve receber uma coleção de números e retornar o
	 * menor
	 * 
	 * @param numeros
	 * @return
	 */
	public int menorNumero(Integer... numeros) {
		// TODO seu código aqui
		return Integer.MAX_VALUE;
	}

	/**
	 * Exercício 8: Esse método deve receber uma coleção de números e retornar o
	 * a quantidade de vezes que determinado número aparece nessa lista
	 * 
	 * @param numero
	 * @param numeros
	 * @return
	 */
	public int contagemNumerosIguais(int numero, Integer... numeros) {
		// TODO seu código aqui
		return 0;
	}

	/**
	 * Exercício 9: Esse método deve receber uma coleção de números e retornar o
	 * a quantidade de números maiores ao desejado
	 * 
	 * @param numero
	 * @param numeros
	 * @return
	 */
	public int contagemNumerosMaiores(int numero, Integer... numeros) {
		// TODO seu código aqui
		return 0;
	}

	/**
	 * Exercício 10: Esse método deve receber uma coleção de números e retornar
	 * o a quantidade de números menores ao desejado
	 * 
	 * @param numero
	 * @param numeros
	 * @return
	 */
	public int contagemNumerosMenores(int numero, Integer... numeros) {
		// TODO seu código aqui
		return 0;
	}

}